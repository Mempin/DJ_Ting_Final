﻿using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour {

	void Awake(){
		GetComponent<Light>().enabled = false;
	}


	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Mouse1))
		{
			if(GetComponent<Light>().enabled == false)
			{
			
				GetComponent<Light>().enabled = true;

			}
			else
			{
				GetComponent<Light>().enabled = false;
		
			}
		}
	}
}
