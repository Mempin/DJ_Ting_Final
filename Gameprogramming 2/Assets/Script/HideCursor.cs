﻿using UnityEngine;
using System.Collections;

public class HideCursor : MonoBehaviour {

	bool isLocked;

	void Start () {
		SetCursorLock (true);
	}

	void SetCursorLock(bool isLocked)
	{
		this.isLocked = isLocked;
		Cursor.visible = isLocked;
		Cursor.visible = !isLocked;
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			SetCursorLock (!isLocked);
	}
}
